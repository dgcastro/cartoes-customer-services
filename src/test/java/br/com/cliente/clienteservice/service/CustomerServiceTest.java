package br.com.cliente.clienteservice.service;

import br.com.cliente.clienteservice.model.Customer;
import br.com.cliente.clienteservice.repository.CustomerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import java.util.Optional;

@SpringBootTest
public class CustomerServiceTest {
    @MockBean
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerService customerService;

    private Customer customer;

    @BeforeEach
    public void setUp(){
        customer = new Customer();
        customer.setId(1);
        customer.setName("Bobby");
    }

    @Test
    public void saveCustomerTest(){
        Mockito.when(customerRepository.save(Mockito.any(Customer.class))).thenReturn(customer);
        Customer saveClient = customerService.saveCustomer(customer);
        Assertions.assertEquals(customer, saveClient);
    }

    @Test
    public void findCustomerByIdTest(){
        Mockito.when(customerRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(customer));
        Customer clientById = customerService.findCustomer(1);
        Assertions.assertEquals(customer, clientById);
    }

    @Test
    public void findCustomerByIdNotFoundTest(){
        Mockito.when(customerRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(null));
        Assertions.assertThrows(RuntimeException.class, () -> {
            customerService.findCustomer(404);});
    }


}
