package br.com.cliente.clienteservice.repository;

import br.com.cliente.clienteservice.model.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

}
