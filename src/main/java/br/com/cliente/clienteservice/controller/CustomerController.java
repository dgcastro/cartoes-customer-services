package br.com.cliente.clienteservice.controller;

import br.com.cliente.clienteservice.model.Customer;
import br.com.cliente.clienteservice.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.ws.rs.Path;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @PostMapping
    public Customer create(@RequestBody Customer customer){
        return customerService.saveCustomer(customer);
    }

    @GetMapping("/{id}")
    public Customer findById(@PathVariable long id){
        try{
            Customer customer = customerService.findCustomer(id);
            return customer;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }
    @GetMapping("/checkifexists/{id}")
    public boolean checkIfExistsById(@PathVariable long id){
        return customerService.existsById(id);
    }
}
