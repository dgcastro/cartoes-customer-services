package br.com.cliente.clienteservice.service;

import br.com.cliente.clienteservice.model.Customer;
import br.com.cliente.clienteservice.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    public Customer saveCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    public Customer findCustomer(long id) {
        Optional<Customer> optionalCustomer = customerRepository.findById(id);
        if (optionalCustomer.isPresent()){
            return optionalCustomer.get();
        }else{
            throw new RuntimeException("Customer not flound");
        }
    }

    public boolean existsById(long id){
        if(customerRepository.existsById(id)){
            return true;
        }
        return false;
    }
}
